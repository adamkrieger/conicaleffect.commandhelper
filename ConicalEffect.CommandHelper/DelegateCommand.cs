﻿using System;
using System.Windows.Input;

namespace ConicalEffect.CommandHelper
{
	public class DelegateCommand : ICommand
	{
		private readonly Func<object, bool> _canExecute;
		private readonly Action<object> _executeAction;

		public DelegateCommand(Action<object> executeAction)
			: this(executeAction, null)
		{
		}

		public DelegateCommand(Action<object> executeAction, Func<object, bool> canExecute)
		{
			if (executeAction == null)
			{
				throw new ArgumentNullException("executeAction");
			}
			_executeAction = executeAction;
			_canExecute = canExecute;
		}

		public bool CanExecute(object parameter)
		{
			var result = true;
			var canExecuteHandler = _canExecute;
			if (canExecuteHandler != null)
			{
				result = canExecuteHandler(parameter);
			}

			return result;
		}

		public event EventHandler CanExecuteChanged;

		public void Execute(object parameter)
		{
			_executeAction(parameter);
		}

		public void RaiseCanExecuteChanged()
		{
			var handler = CanExecuteChanged;
			if (handler != null)
			{
				handler(this, new EventArgs());
			}
		}
	}
}